#include <iostream>
#include "string"
#include "vector"
#include <stdexcept>
#include "functional"
#include "iomanip"
#include "cmath"

#define GNUPLOT_NAME "gnuplot -persist"

using namespace std;


#ifdef WIN32
#define GNUPLOT_NAME "C:\\gnuplot\\bin\\gnuplot -persist"
#else
#define GNUPLOT_NAME "gnuplot -persist"
#endif

struct Linear {
protected:
    virtual ostream &print(ostream &os) const = 0;

    virtual istream &scan(istream &is) = 0;

public:
    friend ostream &operator<<(ostream &os, const Linear &linear) {
        return linear.print(os);
    }

    friend istream &operator>>(istream &is, Linear &linear) {
        return linear.scan(is);
    }
};


class Matrix : public Linear {
public:
    struct Dim {
        int n, m;

        [[nodiscard]] Dim transpose() const { return {m, n}; }

        bool operator==(const Dim &d) const { return d.n == n && d.m == m; }

        bool operator!=(const Dim &d) const { return !(d.n == n && d.m == m); }

        [[nodiscard]] bool isSquare() const { return n == m; }
    };

    Matrix(int n, int m) {
        this->n = n;
        this->m = m;
        values.resize(n, vector<double>(m));
    }

    explicit Matrix(Dim d) {
        this->n = d.n;
        this->m = d.m;
        values.resize(n, vector<double>(m, 0));
    }

    ostream &print(ostream &os) const override {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (values[i][j] == 0) {
                    os << abs(values[i][j]) << " ";
                } else {
                    os << values[i][j] << " ";
                }
            }
            os << "\n";
        }
        return os;
    }

    [[nodiscard]] double getVal(const int i, const int j) const {
        return values[i - 1][j - 1];
    }

    void setVal(const int i, const int j, double v) {
        values[i - 1][j - 1] = v;
    }

    istream &scan(istream &is) override {
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                cin >> values[i][j];
        return is;
    }

    Matrix &operator=(const Matrix &matrix) {
        checkSize(matrix.size());
        values = matrix.values;
        return *this;
    }

    Matrix operator+(Matrix &matrix) const {
        checkSize(matrix.size());
        return applyBinaryOperation(matrix, [](double a, double b) { return a + b; });
    }

    Matrix operator-(Matrix &matrix) const {
        checkSize(matrix.size());
        return applyBinaryOperation(matrix, [](double a, double b) { return a - b; });
    }

    [[nodiscard]] Dim size() const {
        return {n, m};
    }

    [[maybe_unused]] Matrix T() {
        Matrix transposed = Matrix(size().transpose());
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                transposed.values[j][i] = values[i][j];
            }
        }
        return transposed;
    }

    bool operator==(Matrix &matrix) const {
        return size() == matrix.size() && values == matrix.values;
    }

    Matrix operator*(Matrix &matrix) {
        auto dotProd = [](vector<double> a, vector<double> b) {
            double res = 0;
            for (int i = 0; i < a.size(); i++) res += a[i] * b[i];
            return res;
        };
        Matrix prod(n, matrix.m);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < matrix.m; j++) {
                prod.values[i][j] = dotProd(getRowVector(i), matrix.getColVector(j));
            }
        }
        return prod;
    }

    [[nodiscard]] vector<vector<double> > getValues() const {
        return values;
    }

    [[nodiscard]] int findMaxPivot(const int col) const {
        double maxPivot = abs(getVal(col, col));
        int maxPivotInd = col;
        for (int i = col + 1; i <= n; i++) {
            if (maxPivot < abs(getVal(i, col))) {
                maxPivotInd = i;
                maxPivot = abs(getVal(i, col));
            }
        }
        return maxPivotInd;
    }

    vector<double> getMultipliedRowByScalar(int row, double c) {
        vector<double> res(m);
        for (int i = 0; i < m; i++) res[i] = values[row - 1][i] * c;
        return res;
    }

    void subtractVectorFromRow(int row, vector<double> v) {
        for (int i = 0; i < m; i++) {
            values[row - 1][i] -= v[i];
        }
    }

    void setVector(int row, vector<double> v) {
        for (int i = 0; i < m; i++) values[row - 1][i] = v[i];
    }

protected:
    int n, m;
    vector<vector<double> > values;

    void checkSize(const Dim &dim) const {
        if (dim != size())
            throw runtime_error("Error: the dimensional problem occurred");
    }

    Matrix applyBinaryOperation(Matrix &matrix, const function<double(double, double)> &op) const {
        Matrix newMatrix(n, m);
        for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                newMatrix.values[i][j] = op(values[i][j], matrix.values[i][j]);
        return newMatrix;
    }

    vector<double> getRowVector(int row) {
        return values[row];
    }

    vector<double> getColVector(int col) {
        vector<double> c(n);
        for (int i = 0; i < n; i++) {
            c[i] = values[i][col];
        }
        return c;
    }
};


class SquareMatrix : public Matrix {
public:
    SquareMatrix(Matrix mat) : Matrix(mat.size()) {
        if (!mat.size().isSquare()) throw runtime_error("Error: dimensions do not equal");
        values = mat.getValues();
    }

    SquareMatrix(SquareMatrix const &sm) : Matrix(sm.size()) {
        values = sm.getValues();
    };

    explicit SquareMatrix(int n) : Matrix(n, n) {}

    [[maybe_unused]] explicit SquareMatrix(Dim d) : Matrix(d) {
        if (!d.isSquare()) throw runtime_error("Error: dimensions do not equal");
    }

    [[maybe_unused]] double calculateDeterminant();

    pair<SquareMatrix, bool> getUpperBoundMatrix();
};


class IdentityMatrix : public SquareMatrix {
public:
    explicit IdentityMatrix(int n) : SquareMatrix(n) {
        for (int i = 0; i < n; i++) values[i][i] = 1;
    }

    [[maybe_unused]] explicit IdentityMatrix(Dim d) : IdentityMatrix(d.n) {}

protected:
    istream &scan(istream &is) override { return is; }
};


class EliminationMatrix : public IdentityMatrix {
public:
    explicit EliminationMatrix(int n, SquareMatrix &sm, const int i, const int j) : IdentityMatrix(n) {
        if (i == 1) return;
        int upper = i - 1;
        while (upper > 1 && sm.getVal(upper, j) == 0) upper--;
        if (sm.getVal(upper, j) == 0) return;
        setVal(i, upper, -sm.getVal(i, j) / sm.getVal(upper, j));
    }

protected:
    istream &scan(istream &is) override { return is; }

private:
};


class PermutationMatrix : public IdentityMatrix {
public:
    explicit PermutationMatrix(int n, int row1, int row2) : IdentityMatrix(n) {
        values[row1 - 1].swap(values[row2 - 1]);
    }

protected:
    istream &scan(istream &is) override { return is; }
};


pair<SquareMatrix, bool> SquareMatrix::getUpperBoundMatrix() {
    SquareMatrix upperBound(n);
    upperBound = *this;

    int step = 0, permutations = 0;

    for (int j = 1; j <= n; j++) {
        for (int i = j + 1; i <= n; i++) {
            if (j == i - 1) {
                int pivot = i - 1;
                int maxPivot = findMaxPivot(j);

                if (pivot != maxPivot) {
                    PermutationMatrix p(n, pivot, maxPivot);
                    upperBound = p * upperBound;
                    cout << "step #" << ++step << ": permutation\n" << upperBound;
                    permutations++;
                }
            }
            if (getVal(i, j) == 0) continue;
            EliminationMatrix el(n, upperBound, i, j);
            upperBound = el * upperBound;
            cout << "step #" << ++step << ": elimination\n" << upperBound;
        }
    }

    return {upperBound, permutations % 2};
}

[[maybe_unused]] double SquareMatrix::calculateDeterminant() {
    double det = 1;
    auto ub = getUpperBoundMatrix();
    for (int i = 0; i < n; i++) det *= ub.first.values[i][i];
    return ub.second ? -det : det;
}


class AugmentedMatrix : public Linear {
private:
    SquareMatrix *mat1;
    Matrix *mat2;
    int size;

public:
    explicit AugmentedMatrix(SquareMatrix &mat) {
        this->mat1 = new SquareMatrix(mat);
        this->mat2 = new IdentityMatrix(mat.size());
        size = mat.size().n;
    }

    explicit AugmentedMatrix(SquareMatrix &m1, Matrix &m2) {
        this->mat1 = new SquareMatrix(m1);
        this->mat2 = new Matrix(m2);
        size = mat1->size().n;
    }

    ostream &print(ostream &os) const override {
        for (int i = 1; i <= size; i++) {
            for (int j = 1; j <= size; j++) {
                if (mat1->getVal(i, j) == 0) {
                    os << abs(mat1->getVal(i, j)) << ' ';
                } else {
                    os << mat1->getVal(i, j) << ' ';
                }
            }
            cout << '\n';
        }
        for (int j = 1; j <= size; j++) {
            if (mat2->getVal(j, 1) == 0) os << abs(mat2->getVal(j, 1)) << '\n';
            else os << mat2->getVal(j, 1) << '\n';
        }
        return os;
    }

    istream &scan(istream &is) override { return is; }

    void eliminate() {
        gaussEliminationProcessForward();
        gaussEliminationProcessBackwards();
    }

    SquareMatrix getFirstMatrix() { return *mat1; }

    [[maybe_unused]] Matrix getSecondMatrix() { return *mat2; }

private:
    void gaussEliminationProcessForward() {
        for (int j = 1; j <= size; j++) {
            for (int i = j + 1; i <= size; i++) {
                if (j == i - 1) {
                    int pivot = i - 1;
                    int maxPivot = mat1->findMaxPivot(j);
                    if (pivot != maxPivot) {
                        PermutationMatrix p(size, pivot, maxPivot);
                        *mat1 = p * *mat1;
                        *mat2 = p * *mat2;
                    }
                }
                if (mat1->getVal(i, j) == 0) continue;
                EliminationMatrix el(size, *mat1, i, j);
                *mat1 = el * *mat1;
                *mat2 = el * *mat2;
            }
        }
    }

    void gaussEliminationProcessBackwards() {
        for (int col = size; col >= 1; col--) {
            double pivot = mat1->getVal(col, col);
            for (int r = col - 1; r >= 1; r--) {
                mat2->subtractVectorFromRow(r, mat2->getMultipliedRowByScalar(col, mat1->getVal(r, col) / pivot));
                mat1->setVal(r, col, 0);
            }
        }
        for (int i = 1; i <= size; i++) {
            mat2->setVector(i, mat2->getMultipliedRowByScalar(i, 1.0 / mat1->getVal(i, i)));
            mat1->setVal(i, i, 1);
        }
    }
};


class ColumnVector : public Matrix {
public:
    explicit ColumnVector(int n) : Matrix(n, 1) {};

    ColumnVector(const Matrix &m) : Matrix(m.size().n, 1) {
        if (m.size().m != 1) throw runtime_error("Cannot convert matrix to column vector");
        for (int i = 0; i < m.size().n; i++) values[i][0] = m.getVal(i + 1, 1);
    }

    double norm() {
        double v = 0;
        for (int i = 0; i < n; i++) {
            v += values[i][0] * values[i][0];
        }
        return sqrt(v);
    }

    [[nodiscard]] double getVal(const int i) const {
        return values[i - 1][0];
    }

    void setVal(const int i, const double v) {
        values[i - 1][0] = v;
    }

    ColumnVector operator-(ColumnVector v) const {
        checkSize(v.size());
        return (ColumnVector) applyBinaryOperation(v, [](double a, double b) { return a - b; });
    }

    ColumnVector operator+(ColumnVector v) const {
        checkSize(v.size());
        return (ColumnVector) applyBinaryOperation(v, [](double a, double b) { return a + b; });
    }
};

bool diagonalDomination(SquareMatrix &A) {
    for (int i = 1; i <= A.size().n; i++) {
        double sum = 0;
        for (int j = 1; j <= A.size().n; j++) {
            if (i == j) continue;
            sum += A.getVal(i, j);
        }
        if (abs(sum) >= abs(A.getVal(i, i))) return false;
    }
    return true;
}


ColumnVector JacobiMethod(SquareMatrix &A, ColumnVector &b, double c,
                          const function<double(ColumnVector, ColumnVector)> &accuracy) {
    int n = A.size().n;
    if (!diagonalDomination(A)) {
        cout << "The method is not applicable!\n";
        return ColumnVector(n);
    }

    SquareMatrix alpha(n);
    ColumnVector beta(n);
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (i != j) alpha.setVal(i, j, -A.getVal(i, j) / A.getVal(i, i));
            else alpha.setVal(i, j, 0);
        }
        beta.setVal(i, b.getVal(i) / A.getVal(i, i));
    }

    ColumnVector X(n), prevX(n);
    X = beta;

    cout << "alpha:\n" << alpha;
    cout << "beta:\n" << beta;

    int iter = 0;
    double acc;
    do {
        prevX = X;
        X = alpha * X + beta;
        cout << "x(" << iter++ << "):\n";
        cout << prevX;
        acc = accuracy(prevX, X);
        cout << "e: " << acc << '\n';
    } while (acc >= c);

    cout << "x(" << iter << "):\n" << X;
    return X;
}


ColumnVector SeidelMethod(SquareMatrix &A, ColumnVector &b, double c,
                          const function<double(ColumnVector, ColumnVector)> &accuracy) {
    int n = A.size().n;
    if (!diagonalDomination(A)) {
        cout << "The method is not applicable!\n";
        return ColumnVector(n);
    }

    SquareMatrix alpha(n);
    ColumnVector beta(n);
    for (int i = 1; i <= n; i++) {
        for (int j = 1; j <= n; j++) {
            if (i != j) alpha.setVal(i, j, -A.getVal(i, j) / A.getVal(i, i));
            else alpha.setVal(i, j, 0);
        }
        beta.setVal(i, b.getVal(i) / A.getVal(i, i));
    }

    SquareMatrix B(n);
    SquareMatrix C(n);
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= i; j++)
            B.setVal(i, j, alpha.getVal(i, j));

    for (int i = 1; i <= n; i++)
        for (int j = i + 1; j <= n; j++)
            C.setVal(i, j, alpha.getVal(i, j));


    SquareMatrix diff = IdentityMatrix(n) - B;
    AugmentedMatrix augDiff(diff);
    augDiff.eliminate();
    SquareMatrix invDiff = augDiff.getSecondMatrix();

    ColumnVector X(n), prevX(n);
    X = beta;

    cout << "beta:\n" << beta;
    cout << "alpha:\n" << alpha;
    cout << "B:\n" << B;
    cout << "C:\n" << C;
    cout << "I-B:\n" << diff;
    cout << "(I-B)_-1:\n" << invDiff;

    int iter = 0;
    double acc;
    do {
        prevX = X;
        X = ColumnVector(invDiff * C * X) + (ColumnVector) (invDiff * beta);
        cout << "x(" << iter++ << "):\n";
        cout << prevX;
        acc = accuracy(prevX, X);
        cout << "e: " << acc << '\n';
    } while (acc >= c);

    cout << "x(" << iter << "):\n" << X;
    return X;
}


SquareMatrix inverse(SquareMatrix &m) {
    AugmentedMatrix aug(m);
    aug.eliminate();
    return aug.getSecondMatrix();
}

ColumnVector leastSquares(Matrix &dataset, int degree) {
    Matrix A(dataset.size().n, degree + 1);
    for (int i = 0; i < dataset.size().n; i++) {
        for (int j = 0; j < degree + 1; j++) {
            A.setVal(i + 1, j + 1, pow(dataset.getVal(i + 1, 1), j));
        }
    }
    ColumnVector b(dataset.size().n);
    for (int i = 1; i <= dataset.size().n; i++) b.setVal(i, dataset.getVal(i, 2));

    SquareMatrix ATA = A.T() * A;
    SquareMatrix ATAInv = inverse(ATA);

    Matrix ATb = A.T() * b;

    cout << "A:\n" << A;
    cout << "A_T*A:\n" << ATA;
    cout << "(A_T*A)^-1:\n" << ATAInv;
    cout << "A_T*b:\n" << ATb;

    ColumnVector x_(degree + 1);
    x_ = ATAInv * ATb;

    cout << "x~:\n" << x_;
    return x_;
}

void plotLeastSquares(const ColumnVector& X, const ColumnVector& Y, const ColumnVector& Pol, double polPoints = 100) {
#ifdef WIN32
    FILE *pipe = _popen(GNUPLOT_NAME, "w");
#else
    FILE *pipe = popen(GNUPLOT_NAME, "w");
#endif
    int n = X.size().n;
    int degree = Pol.size().n;

    auto calcPolValue = [&](double x) {
        double val = 0;
        for (int i = 0; i < degree; i++)
            val += pow(x, i) * Pol.getVal(i + 1);
        return val;
    };

    if (pipe != NULL) {

        double xMin = INFINITY, yMin = INFINITY, xMax = -INFINITY, yMax = -INFINITY;
        for (int i = 1; i <= n; i++) {
            xMin = min(xMin, X.getVal(i));
            xMax = max(xMax, X.getVal(i));
            yMin = min(yMin, Y.getVal(i));
            yMax = max(yMax, Y.getVal(i));
        }
        double yd = (yMax - yMin) / 10;
        double xd = (xMax - xMin) / 10;

        // Setting plot ranges
        string xrange = "set xrange[" + to_string(xMin - xd) + ":" + to_string(xMax + xd) + "]";
        string yrange = "set yrange[" + to_string(yMin - yd) + ":" + to_string(yMax + yd) + "]";
        fprintf(pipe, "%s\n%s\n", xrange.c_str(), yrange.c_str());


        fprintf(pipe, "%s\n",
                "plot '-' using 1:2 title 'regression' with lines, '-' using 1:2 title 'targets' with points lw 4 pt 4 ps 1 \n");

        double step = (xMax - xMin) / polPoints;
        for (int i = 0; i <= polPoints; i++) {
            double x = xMin + i * step;
            cout << "Pol: " << x << " " << calcPolValue(x) << "\n";
            fprintf(pipe, "%f\t%f\n", x, calcPolValue(x));
        }

        fprintf(pipe, "e\n");

        for (int i = 1; i <= n; i++) fprintf(pipe, "%f\t%f\n", X.getVal(i), Y.getVal(i));

        fprintf(pipe, "e\n");
        fflush(pipe);

#ifdef WIN32
        _pclose(pipe);
#else
        pclose(pipe);
#endif
    } else throw runtime_error("Could not open pipe");
}

int main() {
    int m;
    cin >> m;
    Matrix A(m, 2);
    cin >> A;
    int n;
    cin >> n;
    cout << fixed << setprecision(4);
    auto pol = leastSquares(A, n);


    ColumnVector X(m);
    ColumnVector Y(m);
    for (int i = 1; i <= m; i++) {
        X.setVal(i, A.getVal(i, 1));
        Y.setVal(i, A.getVal(i, 2));
    }

    plotLeastSquares(X, Y, pol);
}